import React from "react";
import {
    Switch,
    Route,
    Redirect,
  } from "react-router-dom";
  import Sliders from "../component/Sliders/";
  import Medalcoin from "../page/Medalcoin/index";
  import Problems from "../page/Problems/index";
  import Roadmap from "../page/Roadmap/index";
  

function Home() {
  return (
    <>
         <Switch>
                <Route exact path="/" component={Sliders} />
                <Route exact path="/Medalcoin" component={Medalcoin} />
                <Route exact path="/Problems" component={Problems} />
                <Route exact path="/Roadmap" component={Roadmap} />
                {/* <Route exact path="/Whitepaper" component={Whitepaper} /> */}
                <Redirect from="redirect" to="/Home" />
        </Switch>
    </>
  );
}

export default Home;