import React from "react";
import Medal from "./section-medal/index";
import Distributions from "./Distributions"
import Footer from "../../component/Footer";
import "./index.scss"


function CompMedal(){
  return(
    <div className="component-medal">
    <Medal />
    <Distributions />
    <Footer />
    </div>
  )
}

 export default CompMedal;