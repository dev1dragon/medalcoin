import React from "react";
import "./index.scss";
import Notfound from "../../../assets/GraficaMedal1.json";
import Lottie from "react-lottie";


const defaultOptions = {
  loop: true,
  autoplaya: true,
  animationData: Notfound,
};

function Medalcoin() {
  return (
    <div className="medalcoin">
      <div className="container-medal">
        <div className="medal">
          <Lottie options={defaultOptions} />
        </div>
        <div className="medal-text">
          <h1>MEDAL COIN</h1>
          <p>
            Medal coin is an ERC 20 governance token based on Ethereum that
            seeks to decentralize and facilitate transactions for the game
            community through a vision: “One token for the gaming universe.”
            <br />
            <br />
            Medal coin’s purpose is to interconnect players, game developers,
            streamers and influencers, NFT platforms, NFT artists, and investors
            in the same ecosystem.
          </p>
        </div>
      </div>
    </div>
  );
}

export default Medalcoin;
