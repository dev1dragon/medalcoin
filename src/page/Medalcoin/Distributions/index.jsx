import React from "react";
import "./index.scss";
import Lottie from 'react-lottie';
import distribution from '../../../assets/Distribution';

const defaultOptions2 = {
  loop: true,
  autoplaya: true,
  animationData: distribution,
};


function Distributions() {
  return (
    <div className="distributions">
      <div className="content-text">
        <span>TOKEN NAME</span>
        <h2>MEDAL COIN</h2>

        <span>SYMBOL</span>
        <h2>MDAL</h2>

        <span>SUPPLY</span>
        <h2>2,000'000,000</h2>

        <span>TECHNOLOGY</span>
        <h2>ERC-20</h2>
      </div>
      <div className="animation">
        <Lottie options={defaultOptions2} />
      </div>
    </div>
  );
}

export default Distributions;
