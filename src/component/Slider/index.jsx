import React, { Component } from "react";

import Notfound from "../../assets/Slide1.json";
import Notfound2 from "../../assets/Slide2.json";
import Notfound3 from "../../assets/Slide3.json";
import Notfound4 from "../../assets/Slide4.json";

import "./index.scss";
import { Carousel } from "react-responsive-carousel";
import Lottie from "react-lottie";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const defaultOptions = {
  autoplay: true,
  animationData: Notfound,
};
const defaultOptions2 = {
  autoplay: true,
  animationData: Notfound2,
};
const defaultOptions3 = {
  autoplay: true,
  animationData: Notfound3,
};
const defaultOptions4 = {
  autoplay: true,
  animationData: Notfound4,
};

class notfound extends Component {
  render() {
    return (
      <div className="container-slider">
        <div className="slider">
          <Carousel>
            <div className="carousels">
              <Lottie options={defaultOptions} />
            </div>
            <div className="carousels">
              <Lottie options={defaultOptions2} />
            </div>
            <div className="carousels">
              <Lottie options={defaultOptions3} />
            </div>
            <div className="carousels">
              <Lottie options={defaultOptions4} />
            </div>
          </Carousel>
        </div>
      </div>
    );
  }
}

export default notfound;
