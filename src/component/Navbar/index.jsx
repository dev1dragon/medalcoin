import React, { useState } from "react";
import { Link } from "react-router-dom";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { NavLink, Nav, NavMenu } from "./NavbarElements";
import { SidebarData } from "./BottomBarData";
import { IconContext } from "react-icons";
import "./index.scss";
import Logo from "../../assets/logo.png";
const Navbar = () => {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);
  // console.log(sidebar);

  return (
    <>
      <Nav>
        <NavLink to="/" className="img-logo">
          <img src={Logo} alt="logo" />
        </NavLink>
        <Link to="#" className="menu-bars-b">
          <FaIcons.FaBars onClick={showSidebar} />
        </Link>
        <NavMenu>
        <NavLink to="/" >
            Home
        </NavLink>
          <NavLink to="/Medalcoin" activeStyle>
            Medalcoin
          </NavLink>
          <NavLink to="/Problems" activeStyle>
            Problems
          </NavLink>
          <NavLink to="/Roadmap" activeStyle>
            Roadmap
          </NavLink>
        </NavMenu>
      </Nav>
      <IconContext.Provider value={{ color: "#fff" }}>
        <div className={sidebar ? "nav-menu active" : "nav-menu"}>
          <ul className="nav-menu-items" onClick={showSidebar}>
            <li className="navbar-toggle">
              <Link to="/#" className="menu-bars-close">
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            <div className="menu-li">
              {SidebarData.map((item, index) => {
                return (
                  <li key={index} className={item.cName}>
                    <Link to={item.path}>
                      {item.icon}
                      <span>{item.title}</span>
                    </Link>
                  </li>
                );
              })}
            </div>
          </ul>
        </div>
      </IconContext.Provider>
    </>
  );
};

export default Navbar;
