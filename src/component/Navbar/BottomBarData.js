
// import * as FaIcons from 'react-icons/fa';
// import * as AiIcons from 'react-icons/ai';
// import * as IoIcons from 'react-icons/io';


export const SidebarData = [
    {
      title: 'Home',
      path: '/',
      cName: 'nav-text'
    },
    {
      title: 'Medalcoin',
      path: '/Medalcoin',
      cName: 'nav-text'
    },
    {
      title: 'Problems',
      path: '/Problems',
      cName: 'nav-text'
    },
    {
      title: 'Roadmap',
      path: '/Roadmap',
      cName: 'nav-text'
    },
    // {
    //   title: 'Support',
    //   path: '/support',
    //   icon: <IoIcons.IoMdHelpCircle />,
    //   cName: 'nav-text'
    // }
  ];