import React from "react";
import Slider from "../Slider/index";
import SliderMovil from "../SliderMovil";


function Sliders(){
    return(
        <div>
          <Slider />
          <SliderMovil /> 
        </div>
    )
}


export default Sliders;