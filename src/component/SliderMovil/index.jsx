import React from "react";
import assets from "../../assets/assets";
import "./index.scss"

function HomeMovil() {
  return (
    <div className="container-slider-movil">
      <div className="slider-1">
        <div className="img-medal">
          <img src={assets.logoMedal} alt="Logo" />
        </div>
        <div className="slider-1-text">
          <span>WELCOME TO MEDALCOIN</span>
          <h1>ONE CURRENCY, MANY GAMES</h1>
          <p>
            Game developers, streamers, NFT platforms, crypto investor
            communities, marketplaces, and the gamer community all together in
            the same ECOSYSTEM, whit an only currency: MEDAL COIN.
          </p>
        </div>
      </div>

      <div className="slider-2">
        <div className="slider-2-text">
          <span>WELCOME TO THE</span>
          <h1>GAMING UNIVERSE</h1>
          <li>
            <img src={assets.diamante} alt="icon diamante 1" />
            Three billion people play video games.
          </li>
          <li>
            <img src={assets.diamante} alt="icon diamante 2" />
            One billion hours played every day..
          </li>
          <li>
            <img src={assets.diamante} alt="icon diamante 3" />
            180 billion dollars industry (2021).
          </li>
          <li>
            <img src={assets.diamante} alt="icon diamante 4" />
            730M people watch streaming games every day.
          </li>
          <li>
            <img src={assets.diamante} alt="icon diamante 5" />
            More than 10.000 video gamer development studios.
          </li>
        </div>
        <div className="img-medal img-grafica">
          <img src={assets.grafica} alt="grafica medalcoin" />
        </div>
      </div>

      <div className="slider-3">
        <div className="slider-3-text">
          <span>THE PROBLEM</span>
          <h1>CHALLENGES</h1>
          <h2>GAMING INDUSTRY</h2>
        </div>
        <div className="problems-m">
          <img src={assets.recurso4} alt="img problems" />
          <div className="problems-titulo">
            <h2>TRANSACTIONAL COST</h2>
            <p>
              Every time a user buys in a game, he pays between 7% and 12% as
              transactional costs, overpricing the product, making it less
              attractive and affordable.
            </p>
          </div>
        </div>
        <div className="problems-m">
          <img src={assets.recurso2} alt="Img problems 1" />
          <div className="problems-titulo">
            <h2>TRANSACTIONAL COST DIFFERENT COINS IN EACH GAME</h2>
            <p>
              Each game has its currency, if a player stops playing the game,
              their balance will be useless and their investment lost.
            </p>
          </div>
        </div>
        <div className="problems-m">
          <img src={assets.recurso6} alt="Img problems 2" />
          <div className="problems-titulo">
            <h2>BANKING</h2>
            <p>
              The regulations for banking a video game are too strict, and
              getting into markets in different countries could be a headache;
              the process is tedious and costly.
            </p>
          </div>
        </div>
      </div>

      <div className="slider-4">
        <div className="slider-4-text">
          <span>THE SOLUTION</span>
          <h2>MEDAL COIN</h2>
          <p>
            Through blockchain technology, Medal coin will be the key to solve
            all the main transactional problems for game developers, players,
            and other components that constitute the gaming universe.
          </p>
        </div>
        <div className="img-solution">
          <img src={assets.img_0} alt="Img problems 3" />
        </div>
      </div>

    </div>
  );
}


export default HomeMovil;