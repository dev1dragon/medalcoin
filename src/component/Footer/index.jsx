import React from "react";
import "./index.scss";
import assets from "../../assets/assets";

const redesLink = [
  { nombre: "fab fa-facebook-square", ruta: "#" },
  {
    nombre: "fab fa-instagram",
    ruta: "https://www.instagram.com/medal.coin/?hl=es",
  },
  { nombre: "fab fa-twitter", ruta: "https://twitter.com/Mdalcoin" },
  {
    nombre: "fab fa-reddit-alien",
    ruta: "https://www.reddit.com/user/Mdalcoin",
  },
  { nombre: "fab fa-github", ruta: "https://github.com/MedalCoin" },
  { nombre: "fab fa-telegram-plane", ruta: "#" },
  { nombre: "fab fa-discord", ruta: "https://discord.gg/CSvGHJ83AA" },
  {
    nombre: "fab fa-linkedin-in",
    ruta: "https://www.linkedin.com/in/medal-coin-70970121a/",
  },
  {
    nombre: "fab fa-slack",
    ruta: "https://join.slack.com/t/medalcoin/shared_invite/zt-uqmxg1at-87IZZWPCv9xltvG41Vq0Og",
  },
];

function Footer() {
  return (
    <div className="container-footer">
      <div className="footer-section">
        <div className="container-privacy">
          <div className="logo-medal">
            <img src={assets.logoM} alt="Mini logo" />
          </div>
          <div className="privacy">
            <span>Privacy</span>
          </div>
        </div>
        <div className="icon-redes">
          <div className="container-redes">
            {redesLink.map((redes) => (
              <a  target="_blank" rel="noopener" href={redes.ruta} key={redes.nombre}>
                <li className={redes.nombre}></li>
              </a>
            ))}
          </div>
        </div>
      </div>
      <div className="footer-description">
        <p>
          Game developers, Streamers, NFTs platforms, Crypto investor
          communities, Marketplaces, and the Gamer community all together in the
          same <strong>ECOSYSTEM</strong>, with an only currency:
          <strong> MEDAL COIN.</strong>
        </p>
      </div>
    </div>
  );
}

export default Footer;
